#!/usr/bin/env bash

set -eu

echo "GRANT SELECT ON mysql.time_zone_name TO '${MYSQL_USER}'@'%';" | mysql -uroot -p"${MYSQL_ROOT_PASSWORD}"
